(** * Prop: Propositions and Evidence *)

Require Export Logic.


Module LeModule.  


(** One useful example is the "less than or equal to"
    relation on numbers. *)

(** The following definition should be fairly intuitive.  It
    says that there are two ways to give evidence that one number is
    less than or equal to another: either observe that they are the
    same number, or give evidence that the first is less than or equal
    to the predecessor of the second. *)

Inductive le : nat -> nat -> Prop :=
  | le_n : forall n, le n n
  | le_S : forall n m, (le n m) -> (le n (S m)).

Notation "m <= n" := (le m n).


(** Proofs of facts about [<=] using the constructors [le_n] and
    [le_S] follow the same patterns as proofs about properties, like
    [ev] in chapter [Prop].  We can [apply] the constructors to prove [<=]
    goals (e.g., to show that [3<=3] or [3<=6]), and we can use
    tactics like [inversion] to extract information from [<=]
    hypotheses in the context (e.g., to prove that [(2 <= 1) -> 2+2=5].) *)

(** *** *)
(** Here are some sanity checks on the definition.  (Notice that,
    although these are the same kind of simple "unit tests" as we gave
    for the testing functions we wrote in the first few lectures, we
    must construct their proofs explicitly -- [simpl] and
    [reflexivity] don't do the job, because the proofs aren't just a
    matter of simplifying computations.) *)

Theorem test_le1 :
  3 <= 3.
Proof.
  (* WORKED IN CLASS *)
  apply le_n.  Qed.

Theorem test_le2 :
  3 <= 6.
Proof.
  (* WORKED IN CLASS *)
  apply le_S. 
  apply le_S. 
  apply le_S. 
  apply le_n.  
Qed.

Theorem test_le3 :
  (2 <= 1) -> 2 + 2 = 5.
Proof. 
  (* WORKED IN CLASS *)
  intros H. inversion H. inversion H2.  Qed.

(** *** *)
(** The "strictly less than" relation [n < m] can now be defined
    in terms of [le]. *)

End LeModule.

Definition lt (n m:nat) := le (S n) m.

Notation "m < n" := (lt m n).



Theorem O_le_n : forall n,
  0 <= n.
Proof.
  intros n.
  induction n.
    apply le_n.
    apply le_S.
    apply IHn.
Qed.


Lemma le_trans : forall m n o, m <= n -> n <= o -> m <= o.
Proof.
  intros m n o Hmn Hno.
  induction Hno.
    apply Hmn.
    apply le_S. apply IHHno.
Qed.

Theorem n_le_m__Sn_le_Sm : forall n m,
  n <= m -> S n <= S m.
Proof.
  intros n m H.
  induction H.
  Case "le_n".
    apply le_n.
  Case "le_S".
    apply le_S.
    apply IHle.
Qed.


Theorem Sn_le_Sm__n_le_m : forall n m,
  S n <= S m -> n <= m.
Proof. 
  intros n m H.
  inversion H.
  apply le_n.
  apply le_trans with (m:=n) (n:=S n) (o:=m).
  apply le_S.
  apply le_n.
  apply H1.
Qed.


Theorem le_plus_l : forall a b,
  a <= a + b.
Proof. 
  intros a b.
  induction a as [|a'].
    simpl.
    apply O_le_n.
    simpl.
    apply n_le_m__Sn_le_Sm.
    apply IHa'.
Qed.

Theorem plus_lt : forall n1 n2 m,
  n1 + n2 < m ->
  n1 < m /\ n2 < m.
Proof. 
  unfold lt.
  intros n1 n2 m H.
  induction H.
  Case "le_n".
    split.
    apply n_le_m__Sn_le_Sm.
    apply le_plus_l.
    apply n_le_m__Sn_le_Sm.
    rewrite -> plus_comm. 
    apply le_plus_l.
  Case "le_S".
    destruct IHle.
    split.
    apply le_S. apply H0.
    apply le_S. apply H1.
Qed.
  

Theorem lt_S : forall n m,
  n < m ->
  n < S m.
Proof.
  unfold lt.
  intros n m H.
  apply le_S.
  apply H.
Qed.

Theorem ble_nat_true : forall n m,
  ble_nat n m = true -> n <= m.
Proof. 
  intros n.
  induction n as [|n'].
  Case "n = 0".
    intros m H.
    apply O_le_n.
  Case "n = S n'".
    intros m H.
    destruct m as [|m'].
      inversion H.
      simpl in H.
      apply n_le_m__Sn_le_Sm.
      apply IHn'.
      apply H.
Qed.

Theorem le_ble_nat : forall n m,
  n <= m ->
  ble_nat n m = true.
Proof.



  intros n m H.
  generalize dependent n.
  induction m as [|m'].
  Case "m = 0".
    intros n H.
    inversion H.
    reflexivity.
  Case "m = S m'".
    intros n H.
    inversion H.
      simpl.
      symmetry.
      apply ble_nat_refl.
      destruct n as [|n'].
        reflexivity.
        simpl.
        apply IHm'.
        apply Sn_le_Sm__n_le_m.
        apply H.
Qed.
    

Theorem ble_nat_true_trans : forall n m o,
  ble_nat n m = true -> ble_nat m o = true -> ble_nat n o = true.                               
Proof.
  intros n m o.
  intros Hnm Hmo.
  apply le_ble_nat.
  apply le_trans with (m:=n) (n:=m) (o:=o).
  apply ble_nat_true.
  apply Hnm.
  apply ble_nat_true.
  apply Hmo.
Qed.

(** **** Exercise: 2 stars, optional (ble_nat_false)  *)
Theorem ble_nat_false : forall n m,
  ble_nat n m = false -> ~(n <= m).
Proof.
  intros n m.
  intros H.
  unfold not.
  intros Hnm.
  apply le_ble_nat in Hnm.
  rewrite -> H in Hnm.
  inversion Hnm.
Qed.


(* ####################################################### *)
(** * Inductively Defined Propositions *)

(** In chapter [Basics] we defined a _function_ [evenb] that tests a
    number for evenness, yielding [true] if so.  We can use this
    function to define the _proposition_ that some number [n] is
    even: *)

Definition even (n:nat) : Prop := 
  evenb n = true.

(** That is, we can define "[n] is even" to mean "the function [evenb]
    returns [true] when applied to [n]."  

    Note that here we have given a name
    to a proposition using a [Definition], just as we have
    given names to expressions of other sorts. This isn't a fundamentally
    new kind of proposition;  it is still just an equality. *)

(** Another alternative is to define the concept of evenness
    directly.  Instead of going via the [evenb] function ("a number is
    even if a certain computation yields [true]"), we can say what the
    concept of evenness means by giving two different ways of
    presenting _evidence_ that a number is even. *)

Inductive ev : nat -> Prop :=
  | ev_0 : ev O
  | ev_SS : forall n:nat, ev n -> ev (S (S n)).


(** The first line declares that [ev] is a proposition -- or,
    more formally, a family of propositions "indexed by" natural
    numbers.  (That is, for each number [n], the claim that "[n] is
    even" is a proposition.)  Such a family of propositions is
    often called a _property_ of numbers.  

    The last two lines declare the two ways to give evidence that a
    number [m] is even.  First, [0] is even, and [ev_0] is evidence
    for this.  Second, if [m = S (S n)] for some [n] and we can give
    evidence [e] that [n] is even, then [m] is also even, and [ev_SS n
    e] is the evidence.
*)


(** **** Exercise: 1 star (double_even)  *)

Theorem double_even : forall n,
  ev (double n).
Proof.
  intros n.
  induction n as [|n'].
  Case "n = 0".
  simpl.
  apply ev_0.
  Case "n = S n'".
  simpl.
  apply ev_SS.
  apply IHn'.
Qed.
(** [] *)



(* ##################################################### *)

(** For [ev], we had already defined [even] as a function (returning a
   boolean), and then defined an inductive relation that agreed with
   it. However, we don't necessarily need to think about propositions
   first as boolean functions, we can start off with the inductive
   definition.
*)

(** As another example of an inductively defined proposition, let's
    define a simple property of natural numbers -- we'll call it
    "[beautiful]." *)

(** Informally, a number is [beautiful] if it is [0], [3], [5], or the
    sum of two [beautiful] numbers.  

    More pedantically, we can define [beautiful] numbers by giving four
    rules:

       - Rule [b_0]: The number [0] is [beautiful].
       - Rule [b_3]: The number [3] is [beautiful]. 
       - Rule [b_5]: The number [5] is [beautiful]. 
       - Rule [b_sum]: If [n] and [m] are both [beautiful], then so is
         their sum. *)

(** We will see many definitions like this one during the rest
    of the course, and for purposes of informal discussions, it is
    helpful to have a lightweight notation that makes them easy to
    read and write.  _Inference rules_ are one such notation: *)
(**
                              -----------                               (b_0)
                              beautiful 0
                              
                              ------------                              (b_3)
                              beautiful 3

                              ------------                              (b_5)
                              beautiful 5    

                       beautiful n     beautiful m
                       ---------------------------                      (b_sum)
                              beautiful (n+m)   
*)

(** *** *)
(** Each of the textual rules above is reformatted here as an
    inference rule; the intended reading is that, if the _premises_
    above the line all hold, then the _conclusion_ below the line
    follows.  For example, the rule [b_sum] says that, if [n] and [m]
    are both [beautiful] numbers, then it follows that [n+m] is
    [beautiful] too.  If a rule has no premises above the line, then
    its conclusion holds unconditionally.

    These rules _define_ the property [beautiful].  That is, if we
    want to convince someone that some particular number is [beautiful],
    our argument must be based on these rules.  For a simple example,
    suppose we claim that the number [5] is [beautiful].  To support
    this claim, we just need to point out that rule [b_5] says so.
    Or, if we want to claim that [8] is [beautiful], we can support our
    claim by first observing that [3] and [5] are both [beautiful] (by
0    rules [b_3] and [b_5]) and then pointing out that their sum, [8],
    is therefore [beautiful] by rule [b_sum].  This argument can be
    expressed graphically with the following _proof tree_: *)
(**
         ----------- (b_3)   ----------- (b_5)
         beautiful 3         beautiful 5
         ------------------------------- (b_sum)
                   beautiful 8   
*)
(** *** *)
(** 
    Of course, there are other ways of using these rules to argue that
    [8] is [beautiful], for instance:
         ----------- (b_5)   ----------- (b_3)
         beautiful 5         beautiful 3
         ------------------------------- (b_sum)
                   beautiful 8   
*)

(** **** Exercise: 1 star (varieties_of_beauty)  *)
(** How many different ways are there to show that [8] is [beautiful]? *)

(* FILL IN HERE *)
(** [] *)

(* ####################################################### *)
(** ** Constructing Evidence *)

(** In Coq, we can express the definition of [beautiful] as
    follows: *)

Inductive beautiful : nat -> Prop :=
  b_0   : beautiful 0
| b_3   : beautiful 3
| b_5   : beautiful 5
| b_sum : forall n m, beautiful n -> beautiful m -> beautiful (n+m).

(** *** *)
(** 
    The rules introduced this way have the same status as proven 
    theorems; that is, they are true axiomatically. 
    So we can use Coq's [apply] tactic with the rule names to prove 
    that particular numbers are [beautiful].  *)

Theorem three_is_beautiful: beautiful 3.
Proof.
   (* This simply follows from the rule [b_3]. *)
   apply b_3.
Qed.

Theorem eight_is_beautiful: beautiful 8.
Proof.
   (* First we use the rule [b_sum], telling Coq how to
      instantiate [n] and [m]. *)
   apply b_sum with (n:=3) (m:=5).
   (* To solve the subgoals generated by [b_sum], we must provide
      evidence of [beautiful 3] and [beautiful 5]. Fortunately we
      have rules for both. *)
   apply b_3.
   apply b_5.
Qed.

(** *** *)
(** As you would expect, we can also prove theorems that have
hypotheses about [beautiful]. *)

Theorem beautiful_plus_eight: forall n, beautiful n -> beautiful (8+n).
Proof.
  intros n B.
  apply b_sum with (n:=8) (m:=n).
  apply eight_is_beautiful.
  apply B.
Qed.

(** **** Exercise: 2 stars (b_times2)  *)
Theorem b_times2: forall n, beautiful n -> beautiful (2*n).
Proof.
  intros n H.
    simpl.
    apply b_sum.
    apply H.
    apply b_sum.
    apply H.
    apply b_0.
Qed.

(** [] *)

Lemma mult_succ_r: forall n m : nat, 
  n * S m = n * m + n.
Proof.
  auto.
Qed.

(** **** Exercise: 3 stars (b_timesm)  *)
Theorem b_timesm: forall n m, beautiful n -> beautiful (m*n).
Proof.
  intros n m H.
  induction m as [|m'].
  Case "m = 0".
    simpl.
    apply b_0.
  Case "m = S m'".
   simpl.
   apply b_sum.
   apply H.
   apply IHm'.
Qed.

(** [] *)


(* ####################################################### *)
(** * Using Evidence in Proofs *)
(** ** Induction over Evidence *)

(** Besides _constructing_ evidence that numbers are beautiful, we can
    also _reason about_ such evidence. *)

(** The fact that we introduced [beautiful] with an [Inductive]
    declaration tells Coq not only that the constructors [b_0], [b_3],
    [b_5] and [b_sum] are ways to build evidence, but also that these
    four constructors are the _only_ ways to build evidence that
    numbers are beautiful. *)

(** In other words, if someone gives us evidence [E] for the assertion
    [beautiful n], then we know that [E] must have one of four shapes:

      - [E] is [b_0] (and [n] is [O]),
      - [E] is [b_3] (and [n] is [3]), 
      - [E] is [b_5] (and [n] is [5]), or 
      - [E] is [b_sum n1 n2 E1 E2] (and [n] is [n1+n2], where [E1] is
        evidence that [n1] is beautiful and [E2] is evidence that [n2]
        is beautiful). *)

(** *** *)    
(** This permits us to _analyze_ any hypothesis of the form [beautiful
    n] to see how it was constructed, using the tactics we already
    know.  In particular, we can use the [induction] tactic that we
    have already seen for reasoning about inductively defined _data_
    to reason about inductively defined _evidence_.

    To illustrate this, let's define another property of numbers: *)

Inductive gorgeous : nat -> Prop :=
  g_0 : gorgeous 0
| g_plus3 : forall n, gorgeous n -> gorgeous (3+n)
| g_plus5 : forall n, gorgeous n -> gorgeous (5+n).

(** **** Exercise: 1 star (gorgeous_tree)  *)
(** Write out the definition of [gorgeous] numbers using inference rule
    notation.
 
(* FILL IN HERE *)
[]
*)


(** **** Exercise: 1 star (gorgeous_plus13)  *)
Theorem gorgeous_plus13: forall n, 
  gorgeous n -> gorgeous (13+n).
Proof.
  intros.
  apply g_plus5.
  apply g_plus5.
  apply g_plus3.
  apply H.
Qed.

(** [] *)

(** *** *)
(** It seems intuitively obvious that, although [gorgeous] and
    [beautiful] are presented using slightly different rules, they are
    actually the same property in the sense that they are true of the
    same numbers.  Indeed, we can prove this. *)


Theorem gorgeous__beautiful_FAILED : forall n, 
  gorgeous n -> beautiful n.
Proof.
   intros. induction n as [| n'].
   Case "n = 0". apply b_0.
   Case "n = S n'". (* We are stuck! *)
Abort.

(** The problem here is that doing induction on [n] doesn't yield a
    useful induction hypothesis. Knowing how the property we are
    interested in behaves on the predecessor of [n] doesn't help us
    prove that it holds for [n]. Instead, we would like to be able to
    have induction hypotheses that mention other numbers, such as [n -
    3] and [n - 5]. This is given precisely by the shape of the
    constructors for [gorgeous]. *)


(** *** *)

(** Let's see what happens if we try to prove this by induction on the evidence [H]
   instead of on [n]. *)

Theorem gorgeous__beautiful : forall n, 
  gorgeous n -> beautiful n.
Proof.
   intros n H.
   induction H as [|n'|n'].
   Case "g_0".
       apply b_0.
   Case "g_plus3". 
       apply b_sum. apply b_3.
       apply IHgorgeous.
   Case "g_plus5".
       apply b_sum. apply b_5. apply IHgorgeous. 
Qed.


(* These exercises also require the use of induction on the evidence. *)

(** **** Exercise: 2 stars (gorgeous_sum)  *)
Theorem gorgeous_sum : forall n m,
  gorgeous n -> gorgeous m -> gorgeous (n + m).
Proof.
  intros.
  induction H as [|n'|n'].
  simpl. apply H0.
  rewrite <- plus_assoc.
  apply g_plus3. apply IHgorgeous.
  apply g_plus5. apply IHgorgeous.
Qed.

(** [] *)

(** **** Exercise: 3 stars, advanced (beautiful__gorgeous)  *)
Theorem beautiful__gorgeous : forall n, beautiful n -> gorgeous n.
Proof.
  intros.
  induction H.
    apply g_0.
    apply g_plus3. apply g_0.
    apply g_plus5. apply g_0.
    apply gorgeous_sum.
    apply IHbeautiful1.
    apply IHbeautiful2.
Qed.

(** [] *)




(** **** Exercise: 3 stars, optional (g_times2)  *)
(** Prove the [g_times2] theorem below without using [gorgeous__beautiful].
    You might find the following helper lemma useful. *)

Lemma helper_g_times2 : forall x y z, x + (z + y) = z + x + y.
Proof.
  intros.
  rewrite -> plus_assoc.
  assert (x + z = z + x).
  apply plus_comm.
  rewrite -> H.
  reflexivity.
Qed.


Theorem g_times2: forall n, gorgeous n -> gorgeous (2*n).
Proof.
   intros n H. simpl. 
   induction H.
     simpl. apply g_0.
     rewrite ->helper_g_times2.
     rewrite ->helper_g_times2.
     rewrite ->helper_g_times2.
     simpl.
     apply g_plus3.
     apply g_plus3.
     rewrite <- plus_assoc.
     apply IHgorgeous.
     rewrite ->helper_g_times2.
     rewrite ->helper_g_times2.
     rewrite ->helper_g_times2.
     simpl.
     apply g_plus5.
     apply g_plus5.
     rewrite <- plus_assoc.
     apply IHgorgeous.
Qed.
(** [] *)



(** Here is a proof that the inductive definition of evenness implies
the computational one. *)

Theorem ev__even : forall n,
  ev n -> even n.
Proof.
  intros n E. induction E as [| n' E'].
  Case "E = ev_0". 
    unfold even. reflexivity.
  Case "E = ev_SS n' E'".  
    unfold even. 
    simpl. 
    apply IHE'.  
Qed.

(** **** Exercise: 1 star (ev__even)  *) 
(** Could this proof also be carried out by induction on [n] instead
    of [E]?  If not, why not? *)

(* FILL IN HERE *)
(** [] *)

(** Intuitively, the induction principle [ev n] evidence [ev n] is
    similar to induction on [n], but restricts our attention to only
    those numbers for which evidence [ev n] could be generated. *)

(** **** Exercise: 1 star (l_fails)  *)
(** The following proof attempt will not succeed.
     Theorem l : forall n,
       ev n.
     Proof.
       intros n. induction n.
         Case "O". simpl. apply ev_0.
         Case "S".
           ...
   Intuitively, we expect the proof to fail because not every
   number is even. However, what exactly causes the proof to fail?

(* FILL IN HERE *)
*)

(** [] *)

(** Here's another exercise requiring induction on evidence. *)
(** **** Exercise: 2 stars (ev_sum)  *)

Theorem ev_sum : forall n m,
   ev n -> ev m -> ev (n+m).
Proof. 
  intros n m.
  intros E1 E2.
  induction E1 as [|n' E1'].
  Case "E1 = ev_0".
    simpl. assumption.
  Case "E1 = ev_SS n' E1'". 
    assert (S (S n') + m = S (S (n' + m))) as H.
      reflexivity.
    rewrite -> H.
    apply ev_SS.
    assumption.
Qed.
(** [] *)



(* ####################################################### *)
(** ** Inversion on Evidence *)


(** Having evidence for a proposition is useful while proving, because we 
   can _look_ at that evidence for more information. For example, consider 
    proving that, if [n] is even, then [pred (pred n)] is
    too.  In this case, we don't need to do an inductive proof.  Instead 
    the [inversion] tactic provides all of the information that we need.

 *)

Theorem ev_minus2: forall n,  ev n -> ev (pred (pred n)). 
Proof.
  intros n E.
  inversion E as [| n' E'].
  Case "E = ev_0". simpl. apply ev_0. 
  Case "E = ev_SS n' E'". simpl. apply E'.  Qed.

(** **** Exercise: 1 star, optional (ev_minus2_n)  *)
(** What happens if we try to use [destruct] on [n] instead of [inversion] on [E]? *)

(** [] *)

(** *** *)
(** Here is another example, in which [inversion] helps narrow down to
the relevant cases. *)

Theorem SSev__even : forall n,
  ev (S (S n)) -> ev n.
Proof.
  intros n E.    
  inversion E as [| n' E']. subst.
  apply E'. Qed.

(** ** The Inversion Tactic Revisited *)

(** These uses of [inversion] may seem a bit mysterious at first.
    Until now, we've only used [inversion] on equality
    propositions, to utilize injectivity of constructors or to
    discriminate between different constructors.  But we see here
    that [inversion] can also be applied to analyzing evidence
    for inductively defined propositions.

    (You might also expect that [destruct] would be a more suitable
    tactic to use here. Indeed, it is possible to use [destruct], but 
    it often throws away useful information, and the [eqn:] qualifier
    doesn't help much in this case.)    

    Here's how [inversion] works in general.  Suppose the name
    [I] refers to an assumption [P] in the current context, where
    [P] has been defined by an [Inductive] declaration.  Then,
    for each of the constructors of [P], [inversion I] generates
    a subgoal in which [I] has been replaced by the exact,
    specific conditions under which this constructor could have
    been used to prove [P].  Some of these subgoals will be
    self-contradictory; [inversion] throws these away.  The ones
    that are left represent the cases that must be proved to
    establish the original goal.

    In this particular case, the [inversion] analyzed the construction
    [ev (S (S n))], determined that this could only have been
    constructed using [ev_SS], and generated a new subgoal with the
    arguments of that constructor as new hypotheses.  (It also
    produced an auxiliary equality, which happens to be useless here.)
    We'll begin exploring this more general behavior of inversion in
    what follows. *)


(** **** Exercise: 1 star (inversion_practice)  *)
Theorem SSSSev__even : forall n,
  ev (S (S (S (S n)))) -> ev n.
Proof.
  intros n.
  intros E.
  inversion E as [| n' E']. subst.
  apply SSev__even in E'.
  assumption.
Qed.

(** The [inversion] tactic can also be used to derive goals by showing
    the absurdity of a hypothesis. *)

Theorem even5_nonsense : 
  ev 5 -> 2 + 2 = 9.
Proof.
  intros E5.
  inversion E5. subst.
  inversion H0. subst.
  inversion H1.
Qed.

(** [] *)

(** **** Exercise: 3 stars, advanced (ev_ev__ev)  *)
(** Finding the appropriate thing to do induction on is a
    bit tricky here: *)

Theorem ev_ev__ev : forall n m,
  ev (n+m) -> ev n -> ev m.
Proof.
  intros n m Enm En.
  induction En as [|n' En'].
  Case "En = ev_0".
    assumption.
  Case "En = ev_SS n' En'".
    assert (S (S n') + m = S (S (n' + m))) as H.
      reflexivity.
    rewrite -> H in Enm.
    inversion Enm as [|nm' Enm']. subst.
    apply IHEn'.
    assumption.
Qed.  
(** [] *)

(** **** Exercise: 3 stars, optional (ev_plus_plus)  *)
(** Here's an exercise that just requires applying existing lemmas.  No
    induction or even case analysis is needed, but some of the rewriting
    may be tedious. *)

Theorem ev_plus_plus : forall n m p,
  ev (n+m) -> ev (n+p) -> ev (m+p).
Proof.
  intros n m p Enm Enp. 
  apply ev_ev__ev with (n:=n+n).
  assert (n + m + (n + p) = n + n + (m + p)).
    rewrite -> plus_assoc.
    assert (n + m + n = n + n + m).
      rewrite -> plus_comm. 
      rewrite -> plus_assoc. 
      reflexivity.
    rewrite -> H. 
    rewrite -> plus_assoc.
    reflexivity.
  rewrite <- H.
  apply ev_sum.
  assumption.
  assumption.
  rewrite <- double_plus.
  apply double_even.
Qed.
(** [] *)


(* ####################################################### *)
(** * Discussion and Variations *)
(** ** Computational vs. Inductive Definitions *)

(** We have seen that the proposition "[n] is even" can be
    phrased in two different ways -- indirectly, via a boolean testing
    function [evenb], or directly, by inductively describing what
    constitutes evidence for evenness.  These two ways of defining
    evenness are about equally easy to state and work with.  Which we
    choose is basically a question of taste.

    However, for many other properties of interest, the direct
    inductive definition is preferable, since writing a testing
    function may be awkward or even impossible.  

    One such property is [beautiful].  This is a perfectly sensible
    definition of a set of numbers, but we cannot translate its
    definition directly into a Coq Fixpoint (or into a recursive
    function in any other common programming language).  We might be
    able to find a clever way of testing this property using a
    [Fixpoint] (indeed, it is not too hard to find one in this case),
    but in general this could require arbitrarily deep thinking.  In
    fact, if the property we are interested in is uncomputable, then
    we cannot define it as a [Fixpoint] no matter how hard we try,
    because Coq requires that all [Fixpoint]s correspond to
    terminating computations.

    On the other hand, writing an inductive definition of what it
    means to give evidence for the property [beautiful] is
    straightforward. *)




(* ####################################################### *)
(** ** Parameterized Data Structures *)

(** So far, we have only looked at propositions about natural numbers. However, 
   we can define inductive predicates about any type of data. For example, 
   suppose we would like to characterize lists of _even_ length. We can 
   do that with the following definition.  *)

Inductive ev_list {X:Type} : list X -> Prop :=
  | el_nil : ev_list []
  | el_cc  : forall x y l, ev_list l -> ev_list (x :: y :: l).

(** Of course, this proposition is equivalent to just saying that the
length of the list is even. *)

Lemma ev_list__ev_length: forall X (l : list X), ev_list l -> ev (length l).
Proof. 
    intros X l H. induction H.
    Case "el_nil". simpl. apply ev_0.
    Case "el_cc".  simpl.  apply ev_SS. apply IHev_list.
Qed.

(** However, because evidence for [ev] contains less information than
evidence for [ev_list], the converse direction must be stated very
carefully. *)

Lemma ev_length__ev_list: forall X n, ev n -> forall (l : list X), n = length l -> ev_list l.
Proof.
  intros X n H. 
  induction H.
  Case "ev_0". intros l H. destruct l.
    SCase "[]". apply el_nil. 
    SCase "x::l". inversion H.
  Case "ev_SS". intros l H2. destruct l. 
    SCase "[]". inversion H2. destruct l.
    SCase "[x]". inversion H2.
    SCase "x :: x0 :: l". apply el_cc. apply IHev. inversion H2. reflexivity.
Qed.
    
(** **** Exercise: 4 stars (palindromes)  *)
(** A palindrome is a sequence that reads the same backwards as
    forwards.

    - Define an inductive proposition [pal] on [list X] that
      captures what it means to be a palindrome. (Hint: You'll need
      three cases.  Your definition should be based on the structure
      of the list; just having a single constructor
        c : forall l, l = rev l -> pal l
      may seem obvious, but will not work very well.)
 
    - Prove [pal_app_rev] that 
       forall l, pal (l ++ rev l).
    - Prove [pal_rev] that 
       forall l, pal l -> l = rev l.
*)

Definition consnoc {X:Type} (x : X) (l : list X) (x' : X) := snoc (x :: l) x'.
 
Inductive pal {X:Type} : list X -> Prop :=
  | pal_nil       : pal []
  | pal_singleton : forall (x : X), pal [x]
  | pal_consnoc   : forall (x : X) (l : list X), pal l -> pal (consnoc x l x).

(*
 
if  l = l' ++ rev l'
    
*)

Theorem pal_app_rev_gen : forall X (l : list X) (pl : list X),
  pal pl -> pal (l ++ pl ++ rev l).
Proof.
  intros X l pl pal_pl.
  induction l as [|h l'].
  Case "l = []". 
    simpl.
    rewrite -> app_nil.
    apply pal_pl.
  Case "l = h :: l'".
    simpl.
    assert (snoc (h :: (l' ++ pl ++ rev l')) h = h :: l' ++ pl ++ snoc (rev l') h).
      simpl.
      rewrite -> snoc_with_append.
      rewrite -> snoc_with_append.    
      reflexivity.
    rewrite <- H.
    apply pal_consnoc.
    apply IHl'.
Qed. 

Theorem pal_app_rev : forall X (l : list X), pal (l ++ rev l).
Proof.
  intros X l.
  assert (l ++ rev l = l ++ [] ++ rev l) as H. reflexivity. rewrite -> H.
  apply pal_app_rev_gen. apply pal_nil.
Qed.

Theorem pal_app_rev_singleton : forall X (l : list X) (x: X),
  pal (l ++ x :: rev l).
Proof.
  intros X l x.
  assert (l ++ x :: rev l = l ++ [x] ++ rev l) as H. reflexivity. rewrite -> H.
  apply pal_app_rev_gen. apply pal_singleton.
Qed.

Theorem pal_rev : forall X (l : list X), 
  pal l -> l = rev l.
Proof.
  intros X l Hl.
  induction Hl as [|x|x l']. 
  Case "[]".
    reflexivity.
  Case "[x]".
    reflexivity.
  Case "x .. l' ..x".
    simpl.
    rewrite -> rev_snoc.
    simpl.
    rewrite <- IHHl.
    reflexivity.
Qed.
(** [] *)

(* Again, the converse direction is much more difficult, due to the
lack of evidence. *)

(** **** Exercise: 5 stars, optional (palindrome_converse)  *)
(** Using your definition of [pal] from the previous exercise, prove
    that
     forall l, l = rev l -> pal l.
*)

Lemma rev_nil : forall X (l : list X), rev l = [] -> l = [].
Proof.
intros X l.
intros H.
destruct l as [|h l'] eqn:HH.
   reflexivity.
   simpl in H.
   destruct (rev l') in H.
     simpl in H.
     inversion H.
     simpl in H.
     inversion H.
Qed.   

Definition min (n m: nat): nat :=
  if ble_nat n m then n else m. 

Lemma min_0 : forall (n: nat), min n 0 = 0.
Proof.
  destruct n as [| n'].
  reflexivity.
  reflexivity.
Qed.

Lemma min_S : forall (n m: nat), min (S n) (S m) = S (min n m).
Proof.
intros n m.
unfold min.
simpl.
destruct (ble_nat n m).
reflexivity.
reflexivity.
Qed.

Lemma ble_nat_swap: forall n m,
  ble_nat n m = false -> ble_nat m n = true.
Proof.
  intros n m H.
  generalize dependent m.
  induction n.
    intros m H.
    inversion H.
    intros m H.
    destruct m.
      reflexivity.
      simpl.
      simpl in H.
      apply IHn.
      apply H.
Qed.

Lemma min_ble_nat : forall (x n m: nat),
  x = min n m -> ble_nat x n = true /\ ble_nat x m = true. 
Proof.
  intros x n m.
  split.
  Case "x <= n".
    unfold min in H. 
    destruct (ble_nat n m) eqn:Hnm.
    SCase "n <= m".
      rewrite -> H.
      symmetry.
      apply ble_nat_refl.
    SCase "n > m".
      rewrite -> H.
      apply ble_nat_swap.
      apply Hnm.  
  Case "x <= m".
    unfold min in H. 
    destruct (ble_nat n m) eqn:Hnm.
    SCase "n <= m".
      rewrite -> H.
      apply Hnm.
    SCase "n > m".
      rewrite -> H.
      symmetry.
      apply ble_nat_refl.
Qed.

Lemma min_lte : forall (x n m : nat),
  x = min n m -> x <= n /\ x <= m.
Proof.
  intros x n m H.
  apply min_ble_nat in H.
  destruct H.
  apply ble_nat_true in H.
  apply ble_nat_true in H0.
  split.
  apply H.
  apply H0.
Qed.

Fixpoint take {X : Type} (n : nat) (l : list X) : list X :=
  match (n, l) with
  | (0, _) => []
  | (S n', []) => []
  | (S n', h :: t) => h :: take n' t
  end.

Fixpoint drop {X: Type} (n : nat) (l : list X) : list X :=
  match (n, l) with
  | (0, l) => l
  | (_, []) => []
  | (S n', h :: t) => drop n' t
  end.

Lemma take_nil : forall X (n: nat),
  take n ([]: list X) = [].
Proof.
  intros.
  simpl.
  destruct n.
  reflexivity.
  reflexivity.
Qed.

Lemma take_length_min : forall X (n: nat) (l: list X),
  length (take n l) = min n (length l).
Proof.
  intros X n l.
  generalize dependent n.
  induction l as [|h l'].
  Case "l = nil".
    intros n.
    simpl.  
    rewrite -> min_0.
    rewrite -> take_nil.
    reflexivity.
  Case "l = h : l'".
    intros n.
    simpl.
    destruct n as [|n'].
    SCase "n = 0".
      reflexivity.
    SCase "n = S n'".
      simpl.  
      rewrite -> min_S.    
      apply f_equal.
      apply IHl'.
Qed.

Lemma drop_nil : forall X (n : nat),
  drop n ([]: list X) = [].
Proof.
  intros.
  simpl.
  destruct n.
  reflexivity.
  reflexivity.
Qed.

Lemma drop_length_max : forall X (n: nat) (l : list X),
  length (drop n l) = max (length l - n) 0.
Proof.
  intros X n l.
  generalize dependent n.
  induction l as [|h l'].
  Case "l = nil".
    intros n.
    simpl.
    rewrite -> drop_nil.
    reflexivity.
  Case "l = h :: l'".
    intros n.
    simpl.
    destruct n as [|n'].
      reflexivity.
      simpl.  
      apply IHl'.
Qed.

Theorem take_length_blt_nat : forall X (n : nat) (l : list X),
  ble_nat (length (take n l)) n = true.
Proof. 
  intros.
  rewrite -> take_length_min.
  remember (min n (length l)) as x.
  apply min_ble_nat in Heqx.
  apply Heqx.
Qed.

Theorem take_length : forall X (n : nat) (l : list X),
  length (take n l) <= n.
Proof.
  intros.
  apply ble_nat_true.
  apply take_length_blt_nat.
Qed.

Theorem take_drop : forall X (n: nat) (l: list X),
  take n l ++ drop n l = l.
Proof.
  intros X n.
  induction n as [| n'].
  Case "n = 0".
    reflexivity.
  Case "n = S n'".
    intros l.
    simpl.
    destruct l as [|h l'].
    SCase "l = nil".
      reflexivity.
    SCase "l = h :: l'".
      simpl.      
      rewrite -> IHn'.
      reflexivity.   
Qed.

Fixpoint halve_nat (n : nat) : nat :=
  match n with
  | 0 => 0
  | 1 => 0
  | S (S n') => S (halve_nat n')
  end.
  


Lemma ble_nat_S : forall n m, ble_nat (S n) (S m) = ble_nat n m. Proof. reflexivity. Qed.

Lemma ble_nat_0 : forall n, ble_nat n 0 = true -> n = 0.
Proof.
  intros n H.
  destruct n.
    reflexivity.
    simpl in H.
    inversion H.
Qed.

Lemma ble_nat_0' : forall n, ble_nat 0 n = true.
Proof.
intros.
reflexivity.
Qed.

(* http://pldev.blogspot.co.uk/2012/02/proving-strong-induction-principle-for.html  *)

Lemma le_S' :
  forall n m : nat,
    n <= S m -> n <= m \/ n = S m.
Proof.
  intros.
  inversion H.
  right. reflexivity.
  left. assumption.
Qed.


Theorem strongind_aux : forall (P : nat -> Prop),
  P 0 ->
  (forall n, (forall m, m <= n -> P m) -> P (S n)) ->
  forall n, (forall m, ((m <= n) -> P m)).
Proof.
  induction n as [ | n' IHn' ].
    intros m H1.
    inversion H1.
    assumption.
    intros.
    assert (m <= n' \/ m = S n'). apply le_S'. assumption.
    inversion H2.
    apply IHn'; assumption.
    rewrite H3.
    apply (H0 n'); assumption.
Qed.


Lemma weaken : forall (P : nat -> Prop),
  (forall n, (forall m, ((m <= n) -> P m))) -> (forall n, P n).
Proof.
  intros P H n.
  apply (H n n).
  apply le_n.
Qed.


Theorem strongind : forall (P : nat -> Prop),
  P 0 ->
  (forall n, (forall m, m <= n -> P m) -> P (S n)) ->
  forall n, P n.
Proof.
  intros.
  apply weaken.
  apply strongind_aux; assumption.
Qed.

Lemma halve_nat_n_le_halve_nat_Sn : forall n,
  halve_nat n <= halve_nat (S n).
Proof.
  apply strongind.
    reflexivity.  
    intros n.
    intros Hind.
    simpl.
    destruct n as [|n'].
      simpl.
      apply O_le_n.
      apply n_le_m__Sn_le_Sm.
      apply Hind.
      apply le_S.
      reflexivity.
Qed.


Lemma halve_nat_n_le_n : forall (n: nat),
 halve_nat n <= n.
Proof.
  intros n.
  induction n as [|n'].
    reflexivity.
    simpl.
    destruct n' as [|n''].
      apply O_le_n.
      apply n_le_m__Sn_le_Sm.
      apply le_trans with (n:=halve_nat (S n'')).
      apply halve_nat_n_le_halve_nat_Sn.
      apply IHn'.
Qed.



Definition first_half { X : Type} (l : list X) : list X := take (halve_nat (length l)) l.

Definition second_half { X : Type} (l : list X) : list X := drop (halve_nat (length l)) l.

Definition second_half' {X} (l : list X) := 
  if evenb (length l) then second_half l
  else drop 1 (second_half l).

Definition middle {X} (l : list X): list X := 
  if evenb (length l) then []
  else take 1 (second_half l).

Lemma middle_length : forall X (l: list X),
  length (middle l) = 0 \/ length (middle l) = 1.
Proof.
  intros.
  unfold middle.
  destruct (evenb (length l)).
    left. reflexivity.
    simpl.
      destruct (second_half l). 
        left. reflexivity.
        right. reflexivity.
Qed.         

Lemma pal_middle : forall X (l: list X), pal (middle l).
  intros X l.
  assert (length (middle l) = 0 \/ length (middle l) = 1).
    apply middle_length.
  destruct H.
    simpl in H.
    apply length_0 in H.
    rewrite -> H.
    constructor.
    destruct (middle l).
      inversion H.
      simpl in H.
      destruct l0.
        constructor.
        simpl in H.
        inversion H.
Qed.


Theorem middle_rev: forall X (l : list X), middle l = rev (middle l).
Proof.
  intros.
  unfold middle.
  destruct (evenb (length l)).
    reflexivity.
    simpl.
    destruct (second_half l) as [|h l'].
      reflexivity.
      reflexivity.
Qed.

Theorem middle_second_half'_second_half : forall X (l : list X),
  second_half l = middle l ++ second_half' l.
Proof.
  intros.
  unfold middle.
  unfold second_half'.
  destruct (evenb (length l)).
    reflexivity.
    rewrite -> take_drop.
    reflexivity.
Qed.

Definition option_to_list {X: Type} (opt: option X): list X :=
  match opt with 
  | Some(x) => [x]
  | None => []
  end.

Inductive od : nat -> Prop :=
  | od_1 : od 1
  | od_SS : forall n:nat, od n -> od (S (S n)).

Definition odd (n:nat) : Prop := 
  oddb n = true.

Lemma od_evenb_false : forall n, od n -> evenb n = false.
Proof.
intros. induction H.
  reflexivity.
  simpl.
  rewrite -> IHod.
  reflexivity.
Qed.

Theorem od__odd : forall n, od n -> odd n.
Proof.
  intros n H.
  unfold odd.
  induction n as [|n'].
    inversion H.
    unfold oddb.
    apply od_evenb_false in H.
    rewrite -> H.
    reflexivity.
Qed.

Lemma od_ev : forall n, ev n -> od (S n).
Proof.  
  intros n H.
  induction H.
  apply od_1.
  apply od_SS.
  apply IHev.
Qed.

Lemma ev_od : forall n, od n -> ev (S n).
Proof.
  intros n H.
  induction H.
    apply ev_SS. apply ev_0.      
    apply ev_SS. apply IHod.
Qed.

Lemma negb_swap_sides : forall b c,
  negb b = c <-> b = negb c.
Proof.  
intros.
split.
Case "->".
intros.
destruct b.
destruct c.
inversion H.
reflexivity.
rewrite <- H.
rewrite -> negb_involutive.
reflexivity.
Case "<-".
intros.
rewrite -> H.
apply negb_involutive.
Qed.

Lemma even_down : forall n, evenb (S n) = false -> evenb n = true.
Proof.
  intros.
  rewrite ->  evenb_n__oddb_Sn.
  rewrite -> H.
  reflexivity.
Qed.

Lemma even_down' : forall n, evenb (S n) = true -> evenb n = false.
Proof.
  intros.
  rewrite ->  evenb_n__oddb_Sn.
  rewrite -> H.
  reflexivity.
Qed.

Lemma even_up : forall n, evenb n = false -> evenb (S n) = true.
Proof.
  intros.
  destruct n as [|n'].
  Case "n = 0".
    inversion H.
  Case "n = S n'".
    simpl.
    apply even_down.
    apply H.
Qed.

Lemma even_up' : forall n, evenb n = true -> evenb (S n) = false.
Proof.
  intros.
  destruct n as [|n'].
  Case "n = 0".
    reflexivity.
  Case "n = S n'".
    simpl.
    apply even_down'.
    apply H.
Qed.

  
Lemma odd_even_flip : forall (n : nat) (b : bool),
  oddb n = b -> evenb n = negb b.
Proof.
  intros.
  apply negb_swap_sides.  
  apply H.
Qed.

Theorem evenodd__evod : forall n,
  (even n -> ev n) /\ (odd n -> od n).
Proof.
  intros n.
  induction n as [|n'].  
  Case "n = 0".
    split. intros. apply ev_0.
           intros. inversion H.
  Case "n = S n'".
    destruct IHn' as [IHev IHod].
    destruct (evenb n') eqn:Heven.
    SCase "evenb n' = true".
      split.
      SSCase "even (S n') -> ev (S n')".
        intros.
        apply even_down' in H.
        rewrite ->H in Heven.
        inversion Heven.
      SSCase "odd (S n') -> od (S n')".
        intros.
        apply od_ev.
        apply IHev.
        apply Heven.
    SCase "evenb n' = false".
      split.
      SSCase "even (S n') -> ev (S n')".
        intros.
        apply ev_od.
        apply IHod.
        apply negb_swap_sides.
        apply Heven.
      SSCase "odd (S n') -> od (S n')".
        intros.
        apply negb_swap_sides in H.
        apply even_up in Heven.
        rewrite ->Heven in H.
        inversion H.
  Show Proof.
Qed.

Theorem even__ev : forall n, even n -> ev n.
Proof.
  intros n.
  apply evenodd__evod.
Qed. 

Theorem odd__od : forall n, odd n -> od n.
Proof.
  intros n.
  apply evenodd__evod.
Qed. 

Lemma snoc_app : forall X (l1 l2: list X) (h: X),
  snoc (l1 ++ l2) h = l1 ++ snoc l2 h.
Proof.
  intros.
  induction l1 as [|l1'].
    reflexivity.
    simpl.
    rewrite -> IHl1.
    reflexivity.
Qed.

Lemma rev_app : forall X (l1 l2: list X),
  rev (l1 ++ l2) = rev l2 ++ rev l1.
Proof.
  intros.
  induction l1 as [|h l1'].
  Case "l1 = nil".
    simpl.
    rewrite -> app_nil.
    reflexivity.
  Case "l1 = h : l1'".
    simpl.
    rewrite -> IHl1'.
    rewrite -> snoc_app.
    reflexivity.
Qed.

Lemma same_length_prefix_equality' : forall {X: Type} (n: nat) (l1 l2 l3 l4: list X),
  length l1 = n ->
  length l3 = n ->
  l1 ++ l2 = l3 ++ l4 ->
  l1 = l3.
Proof.
  intros X n.
  induction n as [| n'].
  Case "n = 0".
    intros l1 l2 l3 l4 Hlen1 Hlen2 Heq.
    apply length_0 in Hlen1.
    apply length_0 in Hlen2.
    subst.
    reflexivity.
  Case "n = S n'".
    intros l1 l2 l3 l4 Hlen1 Hlen2 Heq.
    destruct l1 as [|h1 t1]. 
    SCase "l1 = []".
      destruct l3 as [|h3 t3].
      SSCase "l3 = []". reflexivity.
      SSCase "l3 = h3 : t3". inversion Hlen1.
    SCase "l1 = h1 : t1".
      destruct l3 as [|h3 t3].
      SSCase "l3 = []". inversion Hlen2. 
      SSCase "l3 = h3 : t3".
        inversion Hlen1.
        inversion Hlen2.
        simpl in Heq.
        inversion Heq.
        assert (t1 = t3 -> h3 :: t1 = h3 :: t3). intros. rewrite -> H. reflexivity.
        apply H.          
        apply IHn' with (l2:=l2) (l4:=l4).
        apply H0.
        apply H1.
        apply H3.
Qed.

Lemma same_length_prefix_equality : forall {X: Type} (l1 l2 l3 l4: list X),
  length l1 = length l3 ->
  l1 ++ l2 = l3 ++ l4 ->
  l1 = l3.
Proof.
  intros.
  apply same_length_prefix_equality' with (n:=length l1) (l6:=l2) (l8:=l4).
  reflexivity.
  rewrite -> H. reflexivity.
  assumption.
Qed.

Lemma length_rev : forall X (l: list X),
  length (rev l) = length l.
Proof.
  induction l as [|h l'].
    reflexivity.
    simpl.
    apply length_snoc'.
    apply IHl'.
Qed.

Lemma ev_S_to_od : forall n,
  ev (S n) -> od n.
Proof.
  intros n H.
  apply ev__even in H.  
  unfold even in H.
  apply even_down' in H.
  apply odd__od.
  unfold odd.
  unfold oddb.
  apply negb_swap_sides.
  simpl.
  apply H.
Qed.  

Lemma ev_Sn_od_n : forall n,
  ev (S n) -> od n.
Proof.
  intros n.
  intros HevSn.
  apply ev__even in HevSn.
  apply odd__od.
  unfold odd.
  unfold oddb.
  unfold even in HevSn.
  apply even_down' in HevSn.
  rewrite -> HevSn.
  reflexivity.
Qed.

Lemma od_Sn_ev_n : forall n,
  od (S n) -> ev n.
Proof.
  intros n.
  intros HodSn.
  apply even__ev.
  apply od__odd in HodSn.
  unfold even.
  unfold odd in HodSn.
  unfold oddb in HodSn.
  apply even_down.
  apply negb_swap_sides in HodSn.
  apply HodSn.
Qed.


Lemma halve_nat_S_both : forall n,
  (ev n -> halve_nat (S n) = halve_nat n) /\
  (od n -> halve_nat (S n) = S (halve_nat n)).
Proof.
  intros n.
  induction n as [|n'].
  Case "n = 0".
    split. intros. reflexivity. intros. inversion H.
  Case "n = S n'".
    intros.
    destruct IHn' as [IHeven IHodd].
    destruct n' as [|n''].
      SCase "n' = 0".
        split. intros. inversion H. intros. reflexivity.
      SCase "n' = S n''".
        split.
        SSCase "n is even".
          intros HEven.
          apply ev_Sn_od_n in HEven.
          apply IHodd in HEven.
          simpl.        
          apply f_equal.
          simpl in HEven.
          inversion HEven.
          reflexivity.
        SSCase "n is odd".
          intros HOdd.
          apply od_Sn_ev_n in HOdd.
          apply IHeven in HOdd.   
          simpl.
          apply f_equal.
          simpl in HOdd.
          inversion HOdd.
          reflexivity.
   
Qed.


Lemma halve_nat_S_ev : forall n,
  ev n -> halve_nat (S n) = halve_nat n.
Proof.
  apply halve_nat_S_both.
Qed.

Lemma halve_nat_S_od : forall n,
  od n -> halve_nat (S n) = S (halve_nat n).
Proof.
  apply halve_nat_S_both.
Qed.

Lemma S_plus_swap : forall n m,
  n + S m = S n + m.
Proof.
  intros.
  simpl.
  symmetry.
  apply plus_n_Sm.   
Qed.

Lemma two_halve_nats : forall n,
  (ev n -> n = halve_nat n + halve_nat n) /\
  (od n -> n = halve_nat n + S (halve_nat n)).
Proof.
  intros n.
  induction n as [|n'].
  Case "n = 0".
    split. reflexivity. intros. inversion H.
  Case "n = S n'".
    destruct IHn' as [IHeven IHodd]. 
    destruct n' as [|n''].
    SCase "n' = 0".
      split. intros. inversion H. intros. reflexivity.
    SCase "n' = S n''".
      split.
      SSCase "n is even".
        intros Heven.
        assert (ev n'') as Heven''.
          apply od_Sn_ev_n.
          apply ev_Sn_od_n.
          apply Heven.
        simpl.
        apply f_equal.
        apply ev_Sn_od_n in Heven.
        apply IHodd in Heven.
        apply halve_nat_S_both in Heven''.
        rewrite -> Heven'' in Heven.
        apply Heven.
      SSCase "n is odd".
        intros Hodd.
        assert (od n'') as Hodd''.
          apply ev_Sn_od_n.
          apply od_Sn_ev_n.
          apply Hodd.
        simpl.
        apply f_equal.
        apply od_Sn_ev_n in Hodd.
        apply IHeven in Hodd.
        apply halve_nat_S_both in Hodd''.
        rewrite <- Hodd''.
        rewrite -> S_plus_swap.
        rewrite <- Hodd''.
        apply Hodd.
Qed.

Lemma minus_n_O: forall n : nat, n = n - 0.
  destruct n as [|n'].
    reflexivity.
    reflexivity.
Qed.


Lemma S_minus_1 : forall n, 0 < n -> n = S (n - 1).
Proof.
  intros.
  destruct n as [|n'].
    inversion H.
    simpl.
    rewrite <- minus_n_O.
    reflexivity.
Qed.

Lemma lt_SnSm_nm : forall n m, S n < S m -> n < m.
Proof.
  intros n m.
  unfold lt.
  intros H.
  induction n as [|n'].
    apply le_S_n.
    apply H.
    apply le_S_n.
    apply H.
Qed.    

Lemma le_S_thing' : forall n m,
 S n <= S m -> n <= S m.
Proof.
  intros.
  apply le_S_n in H.
  apply le_S. 
  apply H.
Qed.


Lemma length_drop_S : forall X (l : list X) (n : nat),
 n < length l -> length (drop n l) = S (length (drop (S n) l)).
Proof.
  intros X l n.
  intros Hn.
  rewrite -> drop_length_max.
  rewrite -> drop_length_max.
  rewrite -> max_l.  
  rewrite -> max_l.  
  generalize dependent n.
  
  induction l as [|h l'].
    intros n Hn.
    inversion Hn.
    intros n Hn.
    simpl.
    destruct n as [|n'].
      rewrite <- minus_n_O.    
      reflexivity.
      simpl in Hn.
      apply IHl'.
      apply lt_SnSm_nm.
      apply Hn.
    apply O_le_n.
    apply O_le_n.
Qed.

Lemma max_0 : forall n, max n 0 = n.
Proof.
  intros.
  destruct n as [|n'].
    reflexivity.
    reflexivity.
Qed.

Lemma minus_thing_77 : forall m n, m - S n <= m - n.
Proof.
  intros m n.
  generalize dependent m.
  induction n as [|n'].
    intros m.
    rewrite <- minus_n_O.
    destruct m as [|m'].
      reflexivity.
      simpl.
      rewrite <- minus_n_O.
      constructor.
      reflexivity.
    intros.
    destruct m as [|m'].
      reflexivity.
      simpl.
      apply IHn'.
Qed.


Lemma minus_thing_78 : forall m n,  m - n <= S m - n.
Proof.
  intros m n.
  induction n as [|n'].
    simpl.
    rewrite <- minus_n_O.
    constructor.
    reflexivity.
    simpl.
    apply minus_thing_77.
Qed.


Lemma minus_le_thing : forall m n, m - n <= m.
Proof.
  intros m n.
  induction n as [|n'].
    rewrite <- minus_n_O.
    reflexivity.
    destruct m as [|m'].
      reflexivity.
      simpl.
      apply le_trans with (n:= S m' - n').
        apply minus_thing_78.
        apply IHn'.
Qed.

Lemma take_length_sufficient : forall X (n: nat) (l : list X),
  n <= length l -> length (take n l) = n.
Proof.
  intros X n l H.
  generalize dependent n.
  simpl.
  induction l as [|h l'].
    intros n H.
    simpl in H.
    inversion H.
    reflexivity.
    intros n H.
    simpl in H.
    destruct n as [|n'].
      reflexivity.
      simpl.
      apply f_equal.
      apply IHl'.
      apply Sn_le_Sm__n_le_m in H.
      apply H.
Qed.  

Lemma length_first_half : forall X (l : list X),
  length (first_half l) = halve_nat (length l).
Proof.
  intros X l.
  unfold first_half.
  rewrite -> take_length_sufficient.
  reflexivity.
  apply halve_nat_n_le_n.
Qed.

Lemma length_second_half : forall X (l : list X),
  (ev (length l) -> length (second_half l) = halve_nat (length l)) /\
  (od (length l) -> length (second_half l) = S (halve_nat (length l))).
Proof.
  intros X l.
  unfold second_half.
  induction l as [|h l'].
  Case "l = nil".
    split. intros. reflexivity. intros. inversion H.
  Case "l = h :: l'".
    destruct IHl' as [IHeven IHodd].
    assert (length (h :: l') = S (length l')) as Hlen. reflexivity.
    split.
    SCase "len l is even".
      intros HEven.
      simpl in HEven.
      assert (od (length l')) as HOdd. apply ev_Sn_od_n. apply HEven.
      rewrite -> Hlen.
      rewrite -> halve_nat_S_od.
      simpl.
      apply IHodd. 
      apply HOdd.
      apply HOdd.
    SCase "len l is odd".
      intros HOdd.
      simpl in HOdd.
      assert (ev (length l')) as HEven. apply od_Sn_ev_n. apply HOdd.
      rewrite -> Hlen.
      rewrite -> halve_nat_S_ev.
      apply IHeven in HEven.
      destruct (halve_nat (length l')) eqn:Hh.
        simpl. simpl in HEven. apply length_0 in HEven. rewrite -> HEven. reflexivity.
        simpl. 
        rewrite <- HEven.
        apply length_drop_S.
        unfold lt.
        rewrite -> drop_length_max in HEven.
        rewrite <- HEven.
        rewrite -> max_0.
        apply minus_le_thing. 
        assumption.       
Qed.

Lemma drop_1_length_pred : forall X (l : list X),
  length (drop 1 l) = pred (length l).
Proof.
  intros.
  destruct l as [|h l'].
    reflexivity.
    reflexivity.
Qed.


Lemma length_second_half' : forall X (l : list X),
  length (second_half' l) = halve_nat (length l).
Proof.
  intros X l.
  unfold second_half'.
  destruct (evenb (length l)) eqn:Heven.
  Case "even length".
    apply length_second_half.
    apply even__ev.
    assumption.
  Case "odd length".
    assert (od (length l)). apply odd__od. unfold odd. unfold oddb. rewrite -> Heven. reflexivity.
    apply length_second_half in H.
    rewrite -> drop_1_length_pred.
    rewrite -> H.
    reflexivity.
Qed.      

Lemma app_length : forall X (l1 l2: list X),
  length (l1 ++ l2) = length l1 + length l2.
Proof.
  intros X l1 l2. 
  induction l1 as [| n l1'].
  Case "l1 = nil".
    reflexivity.
  Case "l1 = cons".
    simpl. rewrite -> IHl1'. reflexivity.  Qed.

Lemma list_halves : forall X (l: list X),
  l = first_half l ++ second_half l.
Proof.
  intros
  unfold first_half.
  unfold second_half.
  symmetry.
  apply take_drop.
Qed.


Theorem app_assoc : forall X (l1 l2 l3 : list X), 
  (l1 ++ l2) ++ l3 = l1 ++ (l2 ++ l3).   
Proof.
  intros X l1 l2 l3. induction l1 as [| n l1'].
  Case "l1 = nil".
    reflexivity.
  Case "l1 = cons n l1'".
    simpl. rewrite -> IHl1'. reflexivity.  Qed.


Lemma list_middle_halves : forall X (l: list X),
  l = first_half l ++ middle l ++ second_half' l.
Proof.
  intros X l.
  rewrite <- middle_second_half'_second_half.
  apply list_halves.
Qed.

Lemma rev_app_3 : forall X (l1 l2 l3 : list X),
  rev (l1 ++ l2 ++ l3) = rev l3 ++ rev l2 ++ rev l1.
Proof.
  intros X l1 l2 l3.
  rewrite -> rev_app.
  rewrite -> rev_app.
  rewrite -> app_assoc.  
  reflexivity.
Qed.

Lemma rev_halves : forall X (l: list X),
  l = rev l -> 
  first_half l = rev (second_half' l).
Proof.
  intros X l Hrev.
  assert (first_half l ++              middle l  ++ second_half' l       = 
          rev (second_half' l) ++ rev (middle l) ++ rev (first_half l)).
    rewrite <- list_middle_halves.
    rewrite <- rev_app_3.
    rewrite <- list_middle_halves.
    apply Hrev.
  rewrite <- middle_rev in H.
  apply same_length_prefix_equality with
                                (l1:=first_half l)
                                (l2:=middle l ++ second_half' l)
                                (l3:=rev (second_half' l)) 
                                (l4:=middle l ++ rev (first_half l)).
  rewrite -> length_first_half.
  rewrite -> length_rev.
  rewrite -> length_second_half'.
  reflexivity.
  apply H.
Qed.

Lemma palindrome_converse : forall {X: Type} (l: list X),
  l = rev l -> pal l.
Proof.
  intros X l.
  intros Hrev.
  assert (l = first_half l ++ middle l ++ second_half' l).
    apply list_middle_halves.
  assert (first_half l = rev (second_half' l)).
    apply rev_halves.
    apply Hrev.
  assert (rev (first_half l) = second_half' l).
    rewrite -> H0.
    rewrite -> rev_involutive.
    reflexivity.
  rewrite <- H1 in H. 
  rewrite -> H.
  apply pal_app_rev_gen.
  apply pal_middle.
Qed.

Lemma od_or_ev : forall n, od n \/ ev n.
Proof.
  intros n.
  destruct (evenb n) eqn:Heven.
  right. apply even__ev. assumption.
  left. apply odd__od. unfold odd. unfold oddb. rewrite -> Heven. reflexivity.
Qed.


(* FILL IN HERE *)
(** [] *)



(* ####################################################### *)
(** ** Relations *)

(** A proposition parameterized by a number (such as [ev] or
    [beautiful]) can be thought of as a _property_ -- i.e., it defines
    a subset of [nat], namely those numbers for which the proposition
    is provable.  In the same way, a two-argument proposition can be
    thought of as a _relation_ -- i.e., it defines a set of pairs for
    which the proposition is provable. *)

(** Here are a few more simple relations on numbers: *)

Inductive square_of : nat -> nat -> Prop :=
  sq : forall n:nat, square_of n (n * n).


Inductive next_nat : nat -> nat -> Prop :=
  | nn : forall n:nat, next_nat n (S n).

Inductive next_even : nat -> nat -> Prop :=
  | ne_1 : forall n, ev (S n) -> next_even n (S n)
  | ne_2 : forall n, ev (S (S n)) -> next_even n (S (S n)).

(** **** Exercise: 2 stars (total_relation)  *)
(** Define an inductive binary relation [total_relation] that holds
    between every pair of natural numbers. *)

Inductive total_relation : nat -> nat -> Prop :=
  tot : forall (n m: nat), total_relation n m.

(** [] *)

(** **** Exercise: 2 stars (empty_relation)  *)
(** Define an inductive binary relation [empty_relation] (on numbers)
    that never holds. *)

Inductive empty_relation : nat -> nat -> Prop :=
.

(** [] *)


(** **** Exercise: 2 stars, optional (le_exercises)  *)
(** Here are a number of facts about the [<=] and [<] relations that
    we are going to need later in the course.  The proofs make good
    practice exercises. *)

(** **** Exercise: 3 stars (R_provability2)  *)
Module R.
(** We can define three-place relations, four-place relations,
    etc., in just the same way as binary relations.  For example,
    consider the following three-place relation on numbers: *)

Inductive R : nat -> nat -> nat -> Prop :=
   | c1 : R 0 0 0 
   | c2 : forall m n o, R m n o -> R (S m) n (S o)
   | c3 : forall m n o, R m n o -> R m (S n) (S o)
(*
   | c4 : forall m n o, R (S m) (S n) (S (S o)) -> R m n o
   | c5 : forall m n o, R m n o -> R n m o*).

Example R112: R 1 1 2.
Proof.
 constructor.
 constructor.
 constructor.
Qed.

(** - Which of the following propositions are provable?
      - [R 1 1 2]
      - [R 2 2 6]

    - If we dropped constructor [c5] from the definition of [R],
      would the set of provable propositions change?  Briefly (1
      sentence) explain your answer.
  
    - If we dropped constructor [c4] from the definition of [R],
      would the set of provable propositions change?  Briefly (1
      sentence) explain your answer.

(* FILL IN HERE *)
[]
*)

(** **** Exercise: 3 stars, optional (R_fact)  *)  
(** Relation [R] actually encodes a familiar function.  State and prove two
    theorems that formally connects the relation and the function. 
    That is, if [R m n o] is true, what can we say about [m],
    [n], and [o], and vice versa?
*)

(* FILL IN HERE *)

Theorem plus_R : forall m n o, R m n o -> m + n = o.
Proof.
  intros m n o H.
  induction H.
    reflexivity.
    simpl.    
    rewrite -> IHR.
    reflexivity.
    rewrite <- plus_n_Sm.    
    apply f_equal.
    apply IHR.
(*
    simpl in IHR.
    inversion IHR.
    rewrite <- plus_n_Sm in H1.
    inversion H1.
    reflexivity.
    rewrite -> plus_comm.
    apply IHR.
*)
Qed.

Lemma R_0nn : forall n, R 0 n n.
Proof.
  intros n.
  induction n. constructor.
    constructor.
    assumption.
Qed.

Lemma R_n0n : forall n, R n 0 n.
Proof.
  intros n.
  induction n. constructor.
    constructor.
    assumption.
Qed.

Lemma plus_equal_0_l : forall m n, m + n = 0 -> m = 0.
Proof.
intros.
  destruct m as [|m'].
    reflexivity.
    inversion H.
Qed.     

Lemma plus_equal_0 : forall m n, m + n = 0 -> m = 0 /\ n = 0.
Proof.
intros.
  split. apply plus_equal_0_l with (n:=n). apply H.
         apply plus_equal_0_l with (n:=m). rewrite -> plus_comm in H. apply H.
Qed.     


Theorem R_plus : forall m n o, m + n = o -> R m n o.
Proof.
  intros m.
  induction m as [|m'].
  Case "m = 0".
    intros n o H.
    rewrite <- H.
    simpl.
    apply R_0nn.     
  Case "m = S m'".
    intros n o H.
    destruct n as [|n'].
    SCase "n = 0".
      rewrite -> plus_0_r in H.
      rewrite <- H.
      constructor.
      apply R_n0n.
    SCase "n = S n'".
      destruct o as [|o'].
        inversion H.
        destruct o' as [|o''].
          inversion H. 
          apply plus_equal_0 in H1.
          inversion H1.
          inversion H2.         
          apply c2.
          apply c3.
          apply IHm'.
          rewrite <- plus_n_Sm in H. 
          simpl in H.
          inversion H.
          reflexivity.
Qed.

(** [] *)

End R.

(** **** Exercise: 4 stars, advanced (subsequence)  *)
(** A list is a _subsequence_ of another list if all of the elements
    in the first list occur in the same order in the second list,
    possibly with some extra elements in between. For example,
    [1,2,3]
    is a subsequence of each of the lists
    [1,2,3]
    [1,1,1,2,2,3]
    [1,2,7,3]
    [5,6,1,9,9,2,7,3,8]
    but it is _not_ a subsequence of any of the lists
    [1,2]
    [1,3]
    [5,6,2,1,7,3,8]

    - Define an inductive proposition [subseq] on [list nat] that
      captures what it means to be a subsequence. (Hint: You'll need
      three cases.)

    - Prove [subseq_refl] that subsequence is reflexive, that is, 
      any list is a subsequence of itself.  

    - Prove [subseq_app] that for any lists [l1], [l2], and [l3], 
      if [l1] is a subsequence of [l2], then [l1] is also a subsequence
      of [l2 ++ l3].

    - (Optional, harder) Prove [subseq_trans] that subsequence is 
      transitive -- that is, if [l1] is a subsequence of [l2] and [l2] 
      is a subsequence of [l3], then [l1] is a subsequence of [l3].  
      Hint: choose your induction carefully!
*)

(*

   subseq [1] [1, 4]
   subseq [2] [2, 5]
   subseq [1,2] [1,4,2,5]
   
   subseq [] [99, 100]
   subseq [1, 2] [99, 100,1,4,2,5]
*)
Inductive subseq {X:Type} : list X -> list X -> Prop :=
   | subs_nil : forall l, subseq [] l
   | subs_cons : forall x l1 l2, subseq l1 l2 -> subseq (x :: l1) (x :: l2)
   | subs_app : forall l1 l2 l3 l4, 
      subseq l1 l2 -> subseq l3 l4 -> subseq (l1 ++ l3) (l2 ++ l4).

Theorem subseq_refl : forall X (l: list X), subseq l l.
Proof.
  induction l as [|h l'].
    constructor.
    constructor.
    apply IHl'.
Qed.

Theorem subseq_app : forall X (l1 l2 l3: list X),
  subseq l1 l2 -> subseq l1 (l2 ++ l3).
Proof.
  intros X l1 l2 l3 H.
  replace l1 with (l1 ++ []).
  constructor.
  apply H.
  constructor.  
  apply app_nil.
Qed.

Theorem subseq_nil : forall X (l: list X),
  subseq l [] -> l = [].
Proof.
  intros X l H.
(*
  inversion H.
    subst.
  induction l as [|h l'].    
    reflexivity.
*)
    admit.    
Qed. 

Theorem subseq_trans : forall X (l1 l2 l3 : list X),
  subseq l1 l2 -> subseq l2 l3 -> subseq l1 l3.
Proof.
  intros X l1 l2 l3 H12 H23.
(*
  generalize dependent l1.
  induction H23.
  Case "H23 = subseq_nil".
    intros l1 H12.
    apply subseq_nil in H12.
    subst.
    constructor.
    intros. 
  induction l2 as [|h l2'].
    apply subseq_nil in H12.
    subst.
    constructor.
    apply IHl2'.
    inversion H12. subst.   
      constructor.
      subst.
  generalize dependent l2.
  induction l1 as [|h l1'].
    intros. constructor.
    intros l2 H12 H23.
 *)
  admit.
Qed.

(** [] *)

(** **** Exercise: 2 stars, optional (R_provability)  *)
(** Suppose we give Coq the following definition:
    Inductive R : nat -> list nat -> Prop :=
      | c1 : R 0 []
      | c2 : forall n l, R n l -> R (S n) (n :: l)
      | c3 : forall n l, R (S n) l -> R n l.
    Which of the following propositions are provable?

    - [R 2 [1,0]]
    - [R 1 [1,2,1,0]]
    - [R 6 [3,2,1,0]]
*)

(** [] *)


(* ##################################################### *)
(** * Programming with Propositions *)

(** As we have seen, a _proposition_ is a statement expressing a factual claim,
    like "two plus two equals four."  In Coq, propositions are written
    as expressions of type [Prop]. . *)

Check (2 + 2 = 4).
(* ===> 2 + 2 = 4 : Prop *)

Check (ble_nat 3 2 = false).
(* ===> ble_nat 3 2 = false : Prop *)

Check (beautiful 8).
(* ===> beautiful 8 : Prop *)

(** *** *)
(** Both provable and unprovable claims are perfectly good
    propositions.  Simply _being_ a proposition is one thing; being
    _provable_ is something else! *)

Check (2 + 2 = 5).
(* ===> 2 + 2 = 5 : Prop *)

Check (beautiful 4).
(* ===> beautiful 4 : Prop *)

(** Both [2 + 2 = 4] and [2 + 2 = 5] are legal expressions
    of type [Prop]. *)

(** *** *)
(** We've mainly seen one place that propositions can appear in Coq: in
    [Theorem] (and [Lemma] and [Example]) declarations. *)

Theorem plus_2_2_is_4 : 
  2 + 2 = 4.
Proof. reflexivity.  Qed.

(** But they can be used in many other ways.  For example, we have also seen that
    we can give a name to a proposition using a [Definition], just as we have
    given names to expressions of other sorts. *)

Definition plus_fact : Prop  :=  2 + 2 = 4.
Check plus_fact.
(* ===> plus_fact : Prop *)

(** We can later use this name in any situation where a proposition is
    expected -- for example, as the claim in a [Theorem] declaration. *)

Theorem plus_fact_is_true : 
  plus_fact.
Proof. reflexivity.  Qed.

(** *** *)
(** We've seen several ways of constructing propositions.  

       - We can define a new proposition primitively using [Inductive].

       - Given two expressions [e1] and [e2] of the same type, we can
         form the proposition [e1 = e2], which states that their
         values are equal.

       - We can combine propositions using implication and
         quantification. *)
(** *** *)
(** We have also seen _parameterized propositions_, such as [even] and
    [beautiful]. *)

Check (even 4).
(* ===> even 4 : Prop *)
Check (even 3).
(* ===> even 3 : Prop *)
Check even. 
(* ===> even : nat -> Prop *)

(** *** *)
(** The type of [even], i.e., [nat->Prop], can be pronounced in
    three equivalent ways: (1) "[even] is a _function_ from numbers to
    propositions," (2) "[even] is a _family_ of propositions, indexed
    by a number [n]," or (3) "[even] is a _property_ of numbers."  *)

(** Propositions -- including parameterized propositions -- are
    first-class citizens in Coq.  For example, we can define functions
    from numbers to propositions... *)

Definition between (n m o: nat) : Prop :=
  andb (ble_nat n o) (ble_nat o m) = true.

(** ... and then partially apply them: *)

Definition teen : nat->Prop := between 13 19.

(** We can even pass propositions -- including parameterized
    propositions -- as arguments to functions: *)

Definition true_for_zero (P:nat->Prop) : Prop :=
  P 0.

(** *** *)
(** Here are two more examples of passing parameterized propositions
    as arguments to a function.  

    The first function, [true_for_all_numbers], takes a proposition
    [P] as argument and builds the proposition that [P] is true for
    all natural numbers. *)

Definition true_for_all_numbers (P:nat->Prop) : Prop :=
  forall n, P n.

(** The second, [preserved_by_S], takes [P] and builds the proposition
    that, if [P] is true for some natural number [n'], then it is also
    true by the successor of [n'] -- i.e. that [P] is _preserved by
    successor_: *)

Definition preserved_by_S (P:nat->Prop) : Prop :=
  forall n', P n' -> P (S n').

(** *** *)
(** Finally, we can put these ingredients together to define
a proposition stating that induction is valid for natural numbers: *)

Definition natural_number_induction_valid : Prop :=
  forall (P:nat->Prop),
    true_for_zero P ->
    preserved_by_S P -> 
    true_for_all_numbers P. 





(** **** Exercise: 3 stars (combine_odd_even)  *)
(** Complete the definition of the [combine_odd_even] function
    below. It takes as arguments two properties of numbers [Podd] and
    [Peven]. As its result, it should return a new property [P] such
    that [P n] is equivalent to [Podd n] when [n] is odd, and
    equivalent to [Peven n] otherwise. *)

Definition combine_odd_even (Podd Peven : nat -> Prop) : nat -> Prop :=
  fun n => if oddb n then Podd n else Peven n.


(** To test your definition, see whether you can prove the following
    facts: *)

Theorem combine_odd_even_intro : 
  forall (Podd Peven : nat -> Prop) (n : nat),
    (oddb n = true -> Podd n) ->
    (oddb n = false -> Peven n) ->
    combine_odd_even Podd Peven n.
Proof.
  intros Podd Peven n Hodd Heven.
  unfold combine_odd_even.
  destruct (oddb n).
    apply Hodd. reflexivity.
    apply Heven. reflexivity.
Qed.

Theorem combine_odd_even_elim_odd :
  forall (Podd Peven : nat -> Prop) (n : nat),
    combine_odd_even Podd Peven n ->
    oddb n = true ->
    Podd n.
Proof.
  intros Podd Peven n Hcomb Hodd.
  unfold combine_odd_even in Hcomb.      
  rewrite -> Hodd in Hcomb.
  assumption.
Qed.

Theorem combine_odd_even_elim_even :
  forall (Podd Peven : nat -> Prop) (n : nat),
    combine_odd_even Podd Peven n ->
    oddb n = false ->
    Peven n.
Proof.
  intros Podd Peven n Hcomb Heven.
  unfold combine_odd_even in Hcomb.
  rewrite -> Heven in Hcomb.
  assumption.
Qed.

(** [] *)

(* ##################################################### *)
(** One more quick digression, for adventurous souls: if we can define
    parameterized propositions using [Definition], then can we also
    define them using [Fixpoint]?  Of course we can!  However, this
    kind of "recursive parameterization" doesn't correspond to
    anything very familiar from everyday mathematics.  The following
    exercise gives a slightly contrived example. *)

(** **** Exercise: 4 stars, optional (true_upto_n__true_everywhere)  *)
(** Define a recursive function
    [true_upto_n__true_everywhere] that makes
    [true_upto_n_example] work. *)

Fixpoint true_upto_n__true_everywhere (n: nat) (prop: nat -> Prop): Prop :=
  match n with 
  | 0 => forall m : nat, prop m
  | S n => prop (S n) -> true_upto_n__true_everywhere n prop
  end.

Example true_upto_n_example :
    (true_upto_n__true_everywhere 3 (fun n => even n))
  = (even 3 -> even 2 -> even 1 -> forall m : nat, even m).
Proof. 
reflexivity.  Qed.


(** [] *)

(** $Date: 2014-12-31 11:17:56 -0500 (Wed, 31 Dec 2014) $ *)


